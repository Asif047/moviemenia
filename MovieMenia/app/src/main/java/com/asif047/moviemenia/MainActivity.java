package com.asif047.moviemenia;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {

    private SweetAlertDialog pDialog;
    private String path, response;
    private OkHttpClient client;

    private ModelMovie modelMovie;
    private ApiCallMovies apiCallMovies;
    private List<ModelMovie.Result> data;

    private RecyclerView recyclerView;
    private RecyclerAdapterMovies recyclerAdapterMovies;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        apiCallMovies = new ApiCallMovies();
        data = new ArrayList<>();

        recyclerView = findViewById(R.id.recyclerview);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        try {
            path = BaseUrl.BASE_URL+"3/discover/movie?api_key=004cbaf19212094e32aa9ef6f6577f22";
            new GetDataFromServer().execute();
        } catch (Exception e) {

        }

    }



    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color
                        .parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e) {

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            recyclerAdapterMovies = new RecyclerAdapterMovies(MainActivity.this,
                                            data);
            recyclerView.setAdapter(recyclerAdapterMovies);


        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {

                client = new OkHttpClient();
                response = apiCallMovies.GET(client, path);

                Log.e("##JSON:", response);

                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelMovie>>() {

                }.getType();

                modelMovie = gson.fromJson(response, ModelMovie.class);

                if(data.isEmpty()) {
                    for(int i = 0; i < modelMovie.getResults().size(); i++) {
                        data.add(modelMovie.getResults().get(i));
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }
    }

}
