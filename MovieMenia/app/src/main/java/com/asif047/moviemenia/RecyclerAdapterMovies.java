package com.asif047.moviemenia;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by admin on 9/6/2018.
 */

public class RecyclerAdapterMovies extends RecyclerView.Adapter<RecyclerAdapterMovies.MyViewHolder> {

    private List<ModelMovie.Result> results;
    private Context context;

    public RecyclerAdapterMovies(Context context, List<ModelMovie.Result> results) {
        this.results = results;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_movie, parent, false);

        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvMovieName.setText(results.get(position).getOriginalTitle());
        holder.tvReleasedDate.setText(results.get(position).getReleaseDate());
        holder.tvOverview.setText(results.get(position).getOverview());

        Picasso.with(context).load("https://image.tmdb.org/t/p/w500"+results
                .get(position).getPosterPath()).into(holder.ivMovie);

    }

    @Override
    public int getItemCount() {
        return results.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivMovie;
        TextView tvMovieName, tvReleasedDate, tvOverview;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivMovie = itemView.findViewById(R.id.image_movie);
            tvMovieName = itemView.findViewById(R.id.textview_movie_name);
            tvReleasedDate = itemView.findViewById(R.id.textview_released_date);
            tvOverview = itemView.findViewById(R.id.textview_overview);
        }
    }


}
